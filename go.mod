module git.scc.kit.edu/KIT-CA/dfn-domain-validation-check

go 1.14

require (
	git.scc.kit.edu/KIT-CA/dfnpki v0.1.2
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-colorable v0.1.8 // indirect
)
