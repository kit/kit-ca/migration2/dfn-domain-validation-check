Check if any of the registered domains of a DFN PKI CA need (re-)validation.

This is a companion tool for [dfn-domain-validation](https://git.scc.kit.edu/KIT-CA/dfn-domain-validation).

# Installation

1. Create a dedicated user. We'll use `automation` here: `adduser automation`
2. Create directory `~/.dfn-pki` for the keys: ``` mkdir ~/.dfn-pki chmod 700
   ~/.dfn-pki # copy tsm.cert tsm.chain tsm.key into ~/.dfn-pki chmod 600
   ~/.dfn-pki/tsm.key ```
3. Install [go](https://golang.org). Fetch and build this software: ``` go get
   -v -u git.scc.kit.edu/KIT-CA/dfn-domain-validation-check ```
4. Copy the contents of `contrib/` to `/etc/systemd/system` on the target
   system (as `root`). Edit `dfn-domain-validation-check.service` and change
   the commandline options to match your CA. Run `systemctl daemon-reload`
   followed by `systemctl enable --now dfn-domain-validation-check.timer`.

# Remarks

See `dfn-domain-validation-check -help` for all available options. Most users
should change `-ca` and `-raid`. Almost all users want to use
`-ca dfn-ca-global-g2` with the correct `-raid` arguments (see the java gui:
the correct number is printed in the roo of the tree view as `… (RA NUMBER)`.
