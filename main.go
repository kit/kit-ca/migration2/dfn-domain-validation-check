package main

import (
	"flag"
	"log"
	"math/rand"
	"net/http"
	"regexp"
	"time"

	"github.com/k0kubun/pp"

	"git.scc.kit.edu/KIT-CA/dfnpki"
)

//const (
//	DebugPrintConfig = `
//Configuration:
//
//`
//)

var config struct {
	CAName                   string
	RAID                     int
	Keyfile                  string
	Certfile                 string
	Chainfile                string
	RenewHoursBeforeDeadline time.Duration
	SendEmailThisOften       time.Duration
	DomainTriesPerRun        uint
	Deadline                 int64
	MailSilence              int64
	MailRegexString          string
	Debug                    bool
}

var EmailMustMatch *regexp.Regexp

func init() {
	var err error

	flag.StringVar(&config.CAName, "ca", "kit-ca-g2", "CA short name (one of these: http://cdp.pca.dfn.de)")
	flag.IntVar(&config.RAID, "raid", 0, "RA ID (the default is usually correct)")
	flag.StringVar(&config.Keyfile, "key", "tsm.key", "Private key file for the Teilnehmerservice-Mitarbeiter certificate")
	flag.StringVar(&config.Certfile, "cert", "tsm.cert", "Certificate file for Teilnehmerservice-Mitarbeiter certificate")
	flag.StringVar(&config.Chainfile, "chain", "tsm.chain", "Trust chain file for Teilnehmerservice-Mitarbeiter certificate (optional)")
	flag.UintVar(&config.DomainTriesPerRun, "max-retries", 3, "Stop renewing a domain after three tries")
	flag.Int64Var(&config.Deadline, "deadline", 90, "Renew domains if expiry happens in less days than this number")
	flag.Int64Var(&config.MailSilence, "mailsilence", 1, "Wait at least this number of days between triggering sending of new emails per domain")
	flag.StringVar(&config.MailRegexString, "mailregex", "", "Only trigger renewal if the validation email address matches this regular expression")
	flag.BoolVar(&config.Debug, "debug", false, "Turn on debugging output")
	flag.Parse()

	// Renew domains 90 days before they expire
	config.RenewHoursBeforeDeadline = time.Hour * 24 * time.Duration(config.Deadline)
	// Send at most one email per day per domain
	config.SendEmailThisOften = time.Hour * 24 * time.Duration(config.MailSilence)
	// compile email regexp
	EmailMustMatch, err = regexp.Compile(config.MailRegexString)
	if err != nil {
		log.Fatalf("💀 unable to compile regular expression »%s«: %s", config.MailRegexString, err)
	}
}

func main() {
	var (
		err           error
		httpclient    *http.Client
		soapclient    dfnpki.SoapClient
		domainCounter = make(map[string]uint)
	)

	if config.Debug {
		log.Println("DEBUG: Configuration:")
		_, _ = pp.Print(config)
		log.Printf("\n\n")
	}

	// build authenticating http client
	httpclient, err = dfnpki.GetAuthHTTPClientFromFiles(config.Keyfile, config.Certfile, config.Chainfile)
	if err != nil {
		log.Fatalf("💀 Unable to build client: %s", err)
	}

	// build dfn api client
	soapclient = dfnpki.SoapClient{
		Client:  httpclient,
		SoapURL: dfnpki.GenerateDomains(config.CAName),
	}

	// process domains until there is no work left
MAINLOOP:
	for {
		extendedDomainInformation, err := dfnpki.ListExtendedDomains(soapclient, config.RAID)
		if err != nil {
			log.Fatalf("💀 unable to retrieve domain list: %s", err)
		}
		if config.Debug {
			log.Printf("DEBUG: currently processing %d domains\n", len(extendedDomainInformation.Domains))
		}

		// loop over all renewal candidates until the list is empty
		var candidates []dfnpki.DomainInformation

		// filter domain list for renewal candidates
		for _, di := range extendedDomainInformation.Domains {
			if di.Filter(func(candidate dfnpki.DomainInformation) bool {
				// only approved domains
				if !candidate.Approved {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because it is not approved", candidate.Name)
					}
					return false
				}
				// only domains that will expire soon
				if time.Until(candidate.ValidUntil) > config.RenewHoursBeforeDeadline {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because the expiration date is too far in the future: %s", candidate.Name, candidate.ValidUntil.Format(time.RFC3339))
					}
					return false
				}
				// only domains that did not send an email recently
				if time.Since(candidate.LastChallengeMailSent) < config.SendEmailThisOften {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because the last email was sent too recently: %s", candidate.Name, candidate.LastChallengeMailSent.Format(time.RFC3339))
					}
					return false
				}
				// only domains that have not been tried often this run
				if domainCounter[candidate.Name] > config.DomainTriesPerRun {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because it failed too often (%d > %d) in this run", candidate.Name, domainCounter[candidate.Name], config.DomainTriesPerRun)
					}
					return false
				}
				// only send challenge emails to certain email addresses
				if !EmailMustMatch.MatchString(candidate.ChallengeMailAddress) {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because the challenge e-mail address »%s« does not match out filter »%s«", candidate.Name, candidate.ChallengeMailAddress, config.MailRegexString)
					}
					return false
				}
				// weird domain records
				if candidate.ChallengeMailAddress == "" {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because the challenge e-mail address is empty", candidate.Name)
					}
					return false
				}
				if candidate.Method == "" {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because the challenge method field is empty", candidate.Name)
					}
					return false
				}
				if candidate.Type == "" {
					if config.Debug {
						log.Printf("DEBUG: Not renewing domain »%s« because the challenge type field is empty", candidate.Name)
					}
					return false
				}
				return true
			}) {
				candidates = append(candidates, di)
			}
			if config.Debug {
				pp.Printf("DEBUG: API response for domain %s:\n\n", di)
			}
		}
		if len(candidates) == 0 {
			break MAINLOOP
		}

		// pick a candidate at random, increment counter
		idx := rand.Intn(len(candidates))
		name := candidates[idx].Name
		Type := candidates[idx].Type
		domainCounter[name] += 1

		// trigger renewal
		_, err = dfnpki.SendChallengeEMail(soapclient, config.RAID, name, Type, extendedDomainInformation.Change)
		if err != nil {
			log.Printf("❌️ unable to trigger renewal for domain %s: %s", name, err)
		} else {
			log.Printf("👌️ triggered renewal for domain %s", name) // ✅
		}
	}

	log.Println("🚀 all domains are processed")
}
