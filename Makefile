PLATFORMS := linux/amd64 windows/amd64 darwin/amd64
deps := $(wildcard *.go)

temp = $(subst /, ,$@)
os = $(word 1, $(temp))
arch = $(word 2, $(temp))

release: $(PLATFORMS)

$(PLATFORMS): $(deps)
	GOOS=$(os) GOARCH=$(arch) go build -ldflags="-s -w" -o dfn-domain-validation-check.$(os).$(arch)

.PHONY: release $(PLATFORMS)

clean:
	rm -f dfn-domain-validation-check.*
